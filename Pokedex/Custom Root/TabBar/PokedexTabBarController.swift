//
//  PokedexTabBarController.swift
//  Pokedex
//
//  Created by Rizal Hidayat on 23/09/22.
//

import UIKit

class PokedexTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        initTabBar()
    }

    
    private func initTabBar() {
        UITabBar.appearance().backgroundColor = .white
        UITabBar.appearance().tintColor = .red
        
        let pokedexView = createPokemonView()
        pokedexView.tabBarItem = .pokedex(title: "Pokedex")

        let myCollectionView = createMyCollectionView()
        myCollectionView.tabBarItem = .collections(title: "My Collections")
        setViewControllers([pokedexView, myCollectionView], animated: false)
         
    }
    
    private func createPokemonView() -> PokedexNavigationController {
        let pokemonViewController = PokemonViewController()
        let pokemonViewModel = PokemonViewModel()
        pokemonViewController.title = "Pokedex"
        pokemonViewController.viewModel = pokemonViewModel
        let navigationController = PokedexNavigationController(rootViewController: pokemonViewController)
        return navigationController
    }
    
    private func createMyCollectionView() -> PokedexNavigationController {
        let collectionViewController = MyCollectionViewController()
        let collectionViewModel = MyCollectionViewModel()
        collectionViewController.title = "My Collection"
        collectionViewController.viewModel = collectionViewModel
        let navigationController = PokedexNavigationController(rootViewController: collectionViewController)
        return navigationController
    }
}

//
//  PokedexTabBarItem.swift
//  Pokedex
//
//  Created by Rizal Hidayat on 23/09/22.
//

import UIKit

extension UITabBarItem {
    static func pokedex(title: String?) -> UITabBarItem {
        let image = UIImage(named: "pokeball-default")
        let selectedImage = UIImage(named: "pokeball-selected")
        let tabBarItem = UITabBarItem()
        tabBarItem.title = title
        tabBarItem.image = image
        tabBarItem.selectedImage = selectedImage
        return tabBarItem
    }
    
    static func collections(title: String?) -> UITabBarItem {
        let image = UIImage(named: "school-bag-default")
        let selectedImage = UIImage(named: "school-bag-selected")
        let tabBarItem = UITabBarItem()
        tabBarItem.title = title
        tabBarItem.image = image
        tabBarItem.selectedImage = selectedImage
        return tabBarItem
    }
}

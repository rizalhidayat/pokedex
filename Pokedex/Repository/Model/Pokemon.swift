//
//  Pokemon.swift
//  Pokedex
//
//  Created by Rizal Hidayat on 23/09/22.
//

import Foundation

struct Pokemon: Codable {
    let id: Int?
    let name: String?
    let weight: Int?
    let height: Int?
    let baseExperience: Int?
    let forms: [Information]?
    let sprite: Sprite?
    let abilities: [Ability]?
    let moves: [Move]?
    let types: [PokemonType]?
    let stats: [Stat]?
    
    private enum CodingKeys: String, CodingKey {
        case id, name, weight, height, forms, abilities, moves, types, stats
        case baseExperience = "base_experience"
        case sprite = "sprites"
    }
    
    struct Sprite: Codable {
        let url: String?

        private enum CodingKeys: String, CodingKey {
            case url = "front_default"
        }
    }

    struct Ability: Codable {
        let ability: Information?
    }

    struct Move: Codable {
        let move: Information?
    }

    struct PokemonType: Codable {
        let type: Information?
    }

    struct Stat: Codable {
        let baseStat: Int?
        let stat: Information?
        
        private enum CodingKeys: String, CodingKey {
            case stat
            case baseStat = "base_stat"
        }
    }
}


//
//  Information.swift
//  Pokedex
//
//  Created by Rizal Hidayat on 23/09/22.
//

import Foundation

struct Information: Codable {
    let name: String?
    let url: String?
}

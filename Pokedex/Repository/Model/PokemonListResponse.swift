//
//  PokemonListResponse.swift
//  Pokedex
//
//  Created by Rizal Hidayat on 26/09/22.
//

import Foundation

struct PokemonListResponse: Codable {
    let count: Int?
    let next: String?
    let previous: String?
    let results: [Information]?
}

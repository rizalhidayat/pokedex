//
//  MyCollection.swift
//  Pokedex
//
//  Created by Rizal Hidayat on 27/09/22.
//

import Foundation

struct MyCollection {
    let nickname: String
    let pokemon: Pokemon
}

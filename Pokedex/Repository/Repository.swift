//
//  Repository.swift
//  Pokedex
//
//  Created by Rizal Hidayat on 26/09/22.
//

import UIKit
import Alamofire
import CoreData

protocol RepositoryProtocol {
    func getPokemonsInformation(for page: Int, size: Int, completion: @escaping (PokemonListResponse?) -> Void)
    func getPokemonDetail(id: Int, completion: @escaping (Pokemon?) -> Void)
    func getCollections(completion: @escaping ([MyCollection]) -> Void)
    func collectPokemon(nickname: String, pokemon: Pokemon)
    func deleteCollection(nickname: String, completion: @escaping () -> Void)
}

class Repository: RepositoryProtocol {
    static let shared = Repository()
    
    private let baseURL = "https://pokeapi.co/api/v2/pokemon"
    
    func getPokemonsInformation(for page: Int, size: Int, completion: @escaping (PokemonListResponse?) -> Void) {
        let offset = (page - 1)  * size
        let fullURL = "\(baseURL)?offset=\(offset)&limit=\(size)"
        AF.request(fullURL).responseDecodable(of: PokemonListResponse.self) { response in
            switch response.result {
            case let .success(data):
                completion(data)
            case let .failure(error):
                print(error.localizedDescription)
            }
        }
    }
    
    func getPokemonDetail(id: Int, completion: @escaping (Pokemon?) -> Void) {
        let fullURL = baseURL + "/\(id)"
        AF.request(fullURL).responseDecodable(of: Pokemon.self) { response in
            switch response.result {
            case let .success(data):
                completion(data)
            case let.failure(error):
                print(error.localizedDescription)
            }
        }
    }
    
    func getCollections(completion: @escaping ([MyCollection]) -> Void) {
        var collections: [MyCollection] = []
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContex = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Collection")
        
        do {
            let result = try managedContex.fetch(fetchRequest)
            for data in result as! [NSManagedObject] {
                guard let nickname = data.value(forKey: "nickname") as? String, let pokemonData = data.value(forKey: "pokemon") as? Data, let pokemon = Utils.getPokemonFromData(data: pokemonData) else { continue }
                let collection = MyCollection(nickname: nickname, pokemon: pokemon)
                collections.append(collection)
            }
            completion(collections)
        } catch {
            print("Failed retrieve")
        }
    }
    
    func collectPokemon(nickname: String, pokemon: Pokemon) {
        guard let appdelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appdelegate.persistentContainer.viewContext
        guard let collectionEntity = NSEntityDescription.entity(forEntityName: "Collection", in: managedContext) else { return }
        let collection = NSManagedObject(entity: collectionEntity, insertInto: managedContext)
        collection.setValue(nickname, forKey: "nickname")
        let pokemonData = Utils.getDataFromPokemon(pokemon: pokemon)
        collection.setValue(pokemonData, forKey: "pokemon")
        do {
            try managedContext.save()
        } catch {
            print("Could not save.")
        }
    }
    
    func deleteCollection(nickname: String, completion: @escaping () -> Void){
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContex = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Collection")
        fetchRequest.predicate = NSPredicate(format: "nickname = %@", nickname)
        do {
            let result = try managedContex.fetch(fetchRequest)
            let objectToDelete = result.first as! NSManagedObject
            managedContex.delete(objectToDelete)
            do {
                try managedContex.save()
                completion()
            } catch {
                print("Failed to delete")
            }
        } catch {
            print("Failed retrieve")
        }
    }
    
}

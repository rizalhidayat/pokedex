//
//  PokemonDetailViewController.swift
//  Pokedex
//
//  Created by Rizal Hidayat on 27/09/22.
//

import UIKit
import Kingfisher

class PokemonDetailViewController: UIViewController {
    @IBOutlet weak var pokemonImageView: UIImageView!
    @IBOutlet weak var pokemonNameLabel: UILabel!
    @IBOutlet weak var pokemonTypeLabel: UILabel!
    @IBOutlet weak var pokemonMovesLabel: UILabel!
    @IBOutlet weak var catchButton: UIButton!
    @IBOutlet weak var releaseButton: UIButton!
    
    var viewModel: PokemonDetailViewModel!
    var callback: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initButtons()
        initObserver()
    }
    
    private func initButtons(){
        catchButton.isHidden = viewModel.isOwned
        releaseButton.isHidden = !viewModel.isOwned
        
        catchButton.addTarget(self, action: #selector(catchPokemon), for: .touchUpInside)
        releaseButton.addTarget(self, action: #selector(releasePokemon), for: .touchUpInside)
    }
    
    private func initObserver(){
        viewModel.pokemon.observe(on: self) { pokemon in
            guard let pokemon = pokemon else { return }
            self.updateInformationView(pokemon: pokemon)
        }
        
        viewModel.isReleased.observe(on: self) { released in
            if !released { return }
            self.navigationController?.popViewController(animated: true)
            self.callback?()
        }
    }
    
    private func updateInformationView(pokemon: Pokemon){
        pokemonNameLabel.text = (viewModel.nickname != nil) ? viewModel.nickname : pokemon.name?.capitalized
        let types: [String]? = pokemon.types?.compactMap{ type in
            if let name = type.type?.name {
                return name
            }
            return nil
        }
        pokemonTypeLabel.text = types?.joined(separator: "\n") ?? "-"
        
        let moves: [String]? = pokemon.moves?.compactMap{ move in
            if let name = move.move?.name {
                return name
            }
            return nil
        }
        pokemonMovesLabel.text = moves?.joined(separator: "\n") ?? "-"
        if let imageUrl = pokemon.sprite?.url, let url = URL(string: imageUrl) {
            pokemonImageView.kf.setImage(with: url, placeholder: UIImage(named: "pokeball-selected"))
        }
    }
    
    private func showCatchAlert(catched: Bool){
        if catched {
            let alert = UIAlertController(title: "Success", message: "Please input name for this pokemon", preferredStyle: .alert)
            let saveAction = UIAlertAction(title: "Save", style: .default) { _ in
                self.viewModel.savePokemon(nickname: alert.textFields?.first?.text ?? "")
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
            alert.addTextField()
            alert.addAction(saveAction)
            alert.addAction(cancelAction)
            present(alert, animated: true)
        } else {
            let alert = UIAlertController(title: "Failed", message: "Oops, you missed. Please catch again", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default)
            alert.addAction(okAction)
            present(alert, animated: true)
        }
    }
    
    @objc
    private func catchPokemon(){
        viewModel.catchPokemon { isSuccess in
            self.showCatchAlert(catched: isSuccess)
        }
    }
    
    @objc
    private func releasePokemon(){
        viewModel.releasePokemon()
    }
}

//
//  PokemonDetailViewModel.swift
//  Pokedex
//
//  Created by Rizal Hidayat on 27/09/22.
//

import CoreData
import UIKit

class PokemonDetailViewModel {
    private let repository = Repository.shared
    
    let pokemon: Observable<Pokemon?> = Observable(nil)
    let isReleased: Observable<Bool> = Observable(false)
    var nickname: String?
    var isOwned: Bool
    
    init(id: Int) {
        self.isOwned = false
        fetchFromService(id: id)
    }
    
    init(collection: MyCollection){
        self.isOwned = true
        self.nickname = collection.nickname
        self.pokemon.value = collection.pokemon
    }
    
    private func fetchFromService(id: Int){
        repository.getPokemonDetail(id: id) { result in
            self.pokemon.value = result
        }
    }
    
    func catchPokemon(completion: @escaping (Bool) -> Void) {
        completion(Bool.random())
    }
    
    func savePokemon(nickname: String) {
        guard let pokemon = pokemon.value else { return }
        repository.collectPokemon(nickname: nickname, pokemon: pokemon)
    }
    
    func releasePokemon(){
        guard let nickname = nickname else { return }
        repository.deleteCollection(nickname: nickname) {
            self.isReleased.value = true
        }
    }
    
    
}

//
//  PokemonCollectionViewCell.swift
//  Pokedex
//
//  Created by Rizal Hidayat on 23/09/22.
//

import UIKit
import Kingfisher

class PokemonCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var pokemonImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        containerView.layer.borderWidth = 2.0
        containerView.layer.borderColor = UIColor.darkGray.cgColor
        containerView.layer.cornerRadius = 10
    }
    
    func configure(id: Int, name: String) {
        idLabel.text = "#\(id)"
        nameLabel.text = name
        let url = URL(string: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/\(id).png")
        pokemonImageView.kf.setImage(with: url, placeholder: UIImage(named: "pokemonball-selected"))
    }

    func configure(collection: MyCollection){
        idLabel.isHidden = true
        nameLabel.text = collection.nickname
        if let imageUrl = collection.pokemon.sprite?.url, let url = URL(string: imageUrl) {
            pokemonImageView.kf.setImage(with: url, placeholder: UIImage(named: "pokemonball-selected"))
        }
    }
}

//
//  PokemonViewController.swift
//  Pokedex
//
//  Created by Rizal Hidayat on 23/09/22.
//

import UIKit

class PokemonViewController: UIViewController {
    @IBOutlet weak var pokemonCollectionView: UICollectionView!
    
    var viewModel: PokemonViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initCollectionView()
        initObserver()
        viewModel.fetchPokemons()
    }
    
    private func initCollectionView(){
        pokemonCollectionView.delegate = self
        pokemonCollectionView.dataSource = self
        pokemonCollectionView.register(UINib(nibName: "PokemonCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PokemonCollectionViewCell")
    }
    
    private func initObserver(){
        viewModel.pokemonInformations.observe(on: self) { _ in
            self.pokemonCollectionView.reloadData()
        }
    }

}

extension PokemonViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.pokemonInformations.value?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PokemonCollectionViewCell", for: indexPath) as? PokemonCollectionViewCell, let name = viewModel.pokemonInformations.value?[indexPath.row].name else { return UICollectionViewCell() }
        cell.configure(id: indexPath.row + 1, name: name.capitalized )
        viewModel.loadNextPage(lastIndex: indexPath.row)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let detailViewController = PokemonDetailViewController()
        let viewModel = PokemonDetailViewModel(id: indexPath.row + 1)
        detailViewController.viewModel = viewModel
        detailViewController.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(detailViewController, animated: true)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace: CGFloat = 15 * (3 + 1)
        let availableWidth = self.view.frame.width - paddingSpace - 4
        let availableHeight = self.view.frame.height - paddingSpace - 4
        let widthPerItem = (availableWidth / 3) + 4
        let heightPerItem = (availableHeight / 4) + 4
        return CGSize(width: widthPerItem, height: heightPerItem)
    }
 
}

//
//  PokemonViewModel.swift
//  Pokedex
//
//  Created by Rizal Hidayat on 26/09/22.
//

import Foundation

class PokemonViewModel {
    private let repository = Repository.shared
    
    //MARK: - Pagination
    private var page = 1
    private var totalPage = 1
    private var canLoadNextPage = false
    private let pageSize = 21
    
    //MARK: - Data
    let pokemonInformations: Observable<[Information]?> = Observable(nil)
    
    private func calculateTotalPage(totalData: Int) {
        totalPage = (totalData % pageSize == 0) ? totalData/pageSize : (totalData/pageSize + 1)
    }
    
    func fetchPokemons(){
        repository.getPokemonsInformation(for: page, size: pageSize) { result in
            if self.page == 1 {
                self.calculateTotalPage(totalData: result?.count ?? 0)
                self.pokemonInformations.value = result?.results
            } else {
                let newInformations = result?.results
                self.pokemonInformations.value?.append(contentsOf: newInformations ?? [])
            }
            self.canLoadNextPage = true
            self.page += 1
        }
    }
    
    func loadNextPage(lastIndex: Int) {
        if page <= totalPage && canLoadNextPage {
            let totalData = (pokemonInformations.value?.count ?? 3) - 3
            if lastIndex == totalData {
                fetchPokemons()
            }
        }
    }
    
    
}

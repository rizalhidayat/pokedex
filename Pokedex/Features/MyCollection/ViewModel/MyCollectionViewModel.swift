//
//  MyCollectionViewModel.swift
//  Pokedex
//
//  Created by Rizal Hidayat on 27/09/22.
//

import Foundation

class MyCollectionViewModel {
    private let repository = Repository.shared
    
    let myCollections: Observable<[MyCollection]> = Observable([])
    
    func fetchCollections(){
        repository.getCollections { collections in
            self.myCollections.value = collections
        }
    }
}

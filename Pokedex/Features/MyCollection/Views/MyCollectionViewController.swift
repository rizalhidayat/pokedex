//
//  MyCollectionViewController.swift
//  Pokedex
//
//  Created by Rizal Hidayat on 23/09/22.
//

import UIKit

class MyCollectionViewController: UIViewController {

    @IBOutlet weak var pokemonCollectionView: UICollectionView!
    
    private let refreshControl = UIRefreshControl()
    
    var viewModel: MyCollectionViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initCollectionView()
        initObserver()
        refreshData()
    }
    
    private func initCollectionView(){
        pokemonCollectionView.setEmptyMessage("You don't have any pokemon 🥲")
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        pokemonCollectionView.addSubview(refreshControl)
        pokemonCollectionView.delegate = self
        pokemonCollectionView.dataSource = self
        pokemonCollectionView.register(UINib(nibName: "PokemonCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PokemonCollectionViewCell")
    }
    
    private func initObserver(){
        viewModel.myCollections.observe(on: self) { collections in
            self.pokemonCollectionView.reloadData()
            self.refreshControl.endRefreshing()
        }
    }
    
    @objc
    private func refreshData(){
        self.refreshControl.beginRefreshing()
        viewModel.fetchCollections()
    }

}

extension MyCollectionViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.myCollections.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PokemonCollectionViewCell", for: indexPath) as? PokemonCollectionViewCell else {
            return UICollectionViewCell()
        }
        cell.configure(collection: viewModel.myCollections.value[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let detailViewController = PokemonDetailViewController()
        detailViewController.callback = {
            self.refreshData()
        }
        let viewModel = PokemonDetailViewModel(collection: viewModel.myCollections.value[indexPath.row])
        detailViewController.viewModel = viewModel
        detailViewController.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(detailViewController, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace: CGFloat = 15 * (3 + 1)
        let availableWidth = self.view.frame.width - paddingSpace - 4
        let availableHeight = self.view.frame.height - paddingSpace - 4
        let widthPerItem = (availableWidth / 3) + 4
        let heightPerItem = (availableHeight / 4) + 4
        return CGSize(width: widthPerItem, height: heightPerItem)
    }
}
